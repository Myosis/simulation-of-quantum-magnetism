import numpy as np
import matplotlib.pyplot as plt

gap = np.load('gap.npy')

fig, ax = plt.subplots()
for i in range(6):
    ax.plot(np.arange(15)+1,gap[:,i],label='Potentiel = {}'.format(i))

ax.set_xlabel('Nombre de particules',fontsize=12)
ax.set_ylabel('Energie du gap',fontsize=12)
plt.legend()
plt.show()
