'''
Simulation du magnetisme quantique
dans le cadre du master matiere quantique a l'universite grenoble-alpes
Auteurs: Baptiste Alperin, Robin Botrel, Yoan Fauvel
Calcul des energies, correlations et densites pour plusieurs parametres
changeant (nombre particules, potentiel)
'''
import main as m
import numpy as np
import matplotlib.pyplot as plt

nbSite = 16
nbPartMax = nbSite
potMax = 6
energies = np.zeros((nbPartMax+1,potMax))
rep1=input("Afficher les énergies (y ou n) ? ")
rep2=input("Afficher les densités et corrélations (y ou n) ? ")
print("\n")
for i in range(nbPartMax+1):
    for j in range(potMax):
        energies[i,j], densities, correlations,fcorrelations = m.main(i,nbSite,j)
        if rep1=='y':
            print("nbPart=",i,"\t","V=",j)
            print("Énergie du fondamental :",np.around(energies[i,j],3))

        if rep2=='y':
            print("Densité par site :",np.around(densities,3))
            print("Matrice de corrélations <n_i.n_j>:","\n",np.around(correlations,3))
            #print('fcore :','\n',np.around(fcorrelations,3))
        if rep1=='y' and rep2=='n':
            print("\n")
        elif rep1=='n' and rep2=='y':
             print("\n")
        elif rep1=='y' and rep2=='y':
             print("\n")
    Avancement= (i/nbPartMax)*100
    print("Avancement :",np.around(Avancement),"%")
    print("\n")

gap = 2*energies - np.roll(energies,-1,axis=0) - np.roll(energies,1,axis=0)
print("Gap de charges : (les lignes correspondent aux nombres de particules variant entre 1 et nbPartMax-1, les colonnes au potentiel)","\n",np.around(gap[1:-1],3))
print("\n")
