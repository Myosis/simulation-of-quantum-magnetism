'''
Simulation du magnetisme quantique
dans le cadre du master matiere quantique a l'universite grenoble-alpes
Auteurs: Baptiste Alperin, Robin Botrel, Yoan Fauvel
Calcul des energies, correlations et densites pour des parametres donnés.
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse.linalg import eigs
from scipy.linalg import eigh

def main(nbPart=2,nbSite=16,pot=0,h=0):
    nbSiteRow = int(np.sqrt(nbSite)+0.1)
    global Kin; Kin = pot/2 #Energie cinétique
    global Pot; Pot = pot #Potentiel de Coulomb
    global H; H=h
    Hamiltonian, nbState, Basis = F_hamiltonian(nbPart,nbSite,nbSiteRow)

    #Lanczos
    EigenEnergies, EigenVectors = eigs(Hamiltonian, 1, None, None, 'SR', None, None, None, 1.e-5)
    #Brute Force
    #EigenEnergies, EigenVectors = eigh(Hamiltonian)

    densities, correlations = F_densities(EigenVectors, nbSite, nbState, Basis)

    fcorrelations = F_fourierCorrelation(correlations,nbSiteRow)


    return (EigenEnergies[0]-H*nbPart,densities,correlations,fcorrelations)

def F_hamiltonian(nbPart,nbSite,nbSiteRow):

    nbState = np.math.factorial(nbSite)//(np.math.factorial(nbSite-nbPart)*np.math.factorial(nbPart))
    Basis = np.zeros((nbState),dtype='uint32')
    Hamiltonian = np.zeros((nbState,nbState))

    nbAcceptedBasis = 0
    for i in range(2**nbSite):
        if bin(i).count('1') == nbPart:
            Basis[nbAcceptedBasis] = i #Basis in binary number
            nbAcceptedBasis +=1

    Base2 = 2**np.arange(nbSite,dtype='uint32')
    LRjump = Base2 + np.roll(Base2.reshape((nbSiteRow,nbSiteRow)),-1,axis=1).flatten()
    UDjump = Base2 + np.roll(Base2,-nbSiteRow)
    Jump = np.unique(np.concatenate((LRjump,UDjump))) #toutes les connections entre deux sites adjacents

    for i in range(nbState):
        for j in range(nbState):
            if bin(Basis[i] ^ Basis[j]).count('1') == 2: #inter b⁺i.bj
                for k in Jump:
                    if (Basis[i] ^ Basis[j]) == k:
                        Hamiltonian[i,j] += -Kin #Coef inter

        for k in Jump: #inter ni.nj
                if (Basis[i] & k) == k:
                    Hamiltonian[i,i] += +Pot #Coef inter

    return (Hamiltonian,nbState,Basis)


def F_densities(EigenVectors, nbSite, nbState, Basis):

    densities = np.zeros((nbSite))
    correlations = np.zeros((nbSite,nbSite))

    for s in range(nbSite):
        for w in range(nbState):
            if ( ( Basis[w] & 1 << s ) != 0 ): # densities on site
                densities[s] +=  EigenVectors[w,0] * np.conj(EigenVectors[w,0])
                for i in range(nbSite): # correlations
                    if ( ( Basis[w] & 1 << i ) != 0 ) :
                        correlations[s,i] += (EigenVectors[w,0] * np.conj(EigenVectors[w,0])) #squared if s==i

    correlations = correlations - densities.reshape((nbSite,1))@densities.reshape((1,nbSite)) # correlations = <ninj>-<ni><nj>
    np.fill_diagonal(correlations,0)

    return (densities,correlations)

def F_fourierCorrelation(correlations,nbSiteRow):
    fcorrelations = np.zeros((nbSiteRow,nbSiteRow))

    for kx in range(nbSiteRow):
        for ky in range(nbSiteRow):
            coef = np.arange(nbSiteRow)
            coef = np.abs((np.exp(-1j*2*np.pi*ky*np.arange(nbSiteRow).reshape((nbSiteRow,1))/nbSiteRow)@np.exp(-1j*2*np.pi*kx*np.arange(nbSiteRow).reshape((1,nbSiteRow))/nbSiteRow)).flatten())**2
            coef = coef.reshape((nbSiteRow**2,1))@coef.reshape((1,nbSiteRow**2))
            fcorrelations[kx,ky] = np.sum(np.sum(correlations*coef))

    return fcorrelations



if __name__ == '__main__':
    main()
